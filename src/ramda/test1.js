var R = require("ramda")

R.filter(R.lte(2))([1, 2, 3, 4, 5])

const a = [{
  num: 1
}, {
  num: 2
}, {
  num: 3
}];
const b = [{
  text: "haha"
}, {
  text: "hoho"
}, {
  text: "hengheng"
}];

//  [{num:1, text:'haha'}, {num:2, text:'hoho'},...]
// R.merge在0.26版本已经过期
const zipAB = R.zipWith((x, y) => ({
  ...x,
  ...y
}));
zipAB(a, b);

const data1 = [{
    id: 1,
    name: "北京市"
  },
  {
    id: 2,
    name: "上海市"
  },
  {
    id: 3,
    name: "常德市"
  }
];

const findByPropId = (id) => R.filter(R.where({
  id: R.equals(id)
}));
findByPropId(3)(data1);


// 3. 给每个属性对应的数组前面添加 ['ID', '日期', '管理单元']
const CARDTITLES = {
  achievement: ["交易额", "消费毛收入"],
  poi: ["门店数", "门店动销率"],
  deal: ["项目数", "项目动销率"]
};
const prependSth = R.map(R.concat(["ID", "日期", "管理单元"]));
prependSth(CARDTITLES)

// 4. 找出对象中的所有的poiid构成的数组
const data4 = {
  status: 200,
  errorMsg: null,
  data: [{
      productId: 9196279,
      productName: "双人套餐",
      pois: [{
          name: "海底捞(望京店)",
          poiid: "00101",
          grossRate: 0.0329,
          netMargin: 0.0254
        },
        {
          name: "海底捞(三里屯店)",
          poiid: "00102",
          grossRate: 0.0227,
          netMargin: 0.0151
        }
      ]
    },
    {
      productId: 8868545,
      productName: "四人套餐",
      pois: [{
        name: "海底捞(五道口店)",
        poiid: "00103",
        grossRate: 0.0413,
        netMargin: 0.0315
      }]
    },
    {
      productId: 7497885,
      productName: "烤鸭5折优惠",
      pois: [{
        name: "全聚德(前门店)",
        poiid: "00201",
        grossRate: 0.032,
        netMargin: 0.0154
      }]
    },
    {
      productId: 7497866,
      productName: "家庭畅享套餐",
      pois: [{
        name: "全聚德(门头沟店)",
        poiid: "00202",
        grossRate: 0.0839,
        netMargin: 0.0357
      }]
    },
    {
      productId: 7497888,
      productName: "精品烤鸭一套",
      pois: [{
        name: "全聚德(旗舰店)",
        poiid: "00203",
        grossRate: "0.0119",
        netMargin: "0.0023"
      }]
    }
  ]
};

const pickPoiid = R.compose(
  R.map(R.prop("poiid")),
  R.flatten,
  R.map(R.prop("pois")),
  R.prop("data")
);
// 简化
const pickPooid2 = R.compose(
  R.pluck("poiid"), // pluck 针对数组，
  R.chain(R.prop("pois")), // chain相当于先map后flatten
  R.prop("data")
);

pickPoiid(data4)
pickPooid2(data4)

const data5 = [
  [{
      name: "海底捞(望京店)",
      poiid: "00101",
      grossRate: "0.0329",
      netMargin: "0.0254"
    },
    {
      name: "海底捞(三里屯店)",
      poiid: "00102",
      grossRate: "0.0227",
      netMargin: "0.0151"
    },
    {
      name: "海底捞(五道口店)",
      poiid: "00103",
      grossRate: "0.0413",
      netMargin: "0.0315"
    }
  ],
  [{
      name: "全聚德(前门店)",
      poiid: "00201",
      grossRate: "0.0320",
      netMargin: "0.0154"
    },
    {
      name: "全聚德(门头沟店)",
      poiid: "00202",
      grossRate: "0.0839",
      netMargin: "0.0357"
    },
    {
      name: "全聚德(旗舰店)",
      poiid: "00203",
      grossRate: "0.0119",
      netMargin: "0.0023"
    }
  ]
];

// 把所有的grossRate和netMargin格式化为百分数
const formatPercent = R.map(
  R.map(
    R.mapObjIndexed((val, key, obj) => {
      return R.includes(key, ["grossRate", "netMargin"]) ?
        `${+val * (100).toFixed(2)}%` :
        val;
    })
  )
);

formatPercent(data5)