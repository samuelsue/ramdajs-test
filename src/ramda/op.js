var R = require('ramda');

/*
* 1. 创建List / Obj
*/
R.of(42)
R.objOf('hello', 'mfer')

/*
* 2. Add sth to List / Obj
*/

// Object: assoc, assocPath, mergeAll
R.assoc('a', 123)({ b: 321, c: 213 });
R.assocPath(['a', 'b', 'c'], "kool")({ hello: 'mf' });
R.mergeAll([{ a: 123 }, { b: 234 }, { c: 345 }]);
R.mergeDeepWith(R.concat)(
  { a: 1, c: { values: [1, 2, 3] } },
  { b: 2, c: { values: [4, 5, 6] } }
);

// List: insert insertAll
R.insert(0, 'new')(['bee']);
R.insertAll(0, ['new', 'bee'])(['a']);

/*
* 3. Remove sth from Obj / List
*/

// Obj: dissoc dissocPath, omit
R.omit(['name', 'age'])({ name: 'samuel', age: 26, gender: 'male' });

// List: drop*, filter*, reject, without uniq*
R.drop(1, [1, 2, 3]);
R.dropLast(1, [1, 2, 3]);
// 删除连续的重复元素
R.dropRepeats([1, 1, 1, 2, 2, 2, 3, 2, 3]);
// 从前往后，删除知道遇到false
R.dropWhile(x => x < 2)([1, 2, 3, 4]);
// By 表示单一属性的比较
R.uniqBy(R.toString)([1, '1'])
R.uniqBy(R.compose(
  Math.abs,
  R.curry(parseInt)(R.__, 10)
))(['1', '-1', '-1.23', '1.23456235']);
// with表示更一般的函数
R.uniqWith(R.eqBy(R.toString))(['1', 1, 1]);

// A 里面 删除 B的同样的元素
R.difference([1, 2, 3, 4], [3, 4, 5, 6]);
// B 里面 删除 A的元素
R.without([1, 2, 4], [1, 1, 2, 3, 2, 5]);

// filter 和 reject 互为补操作
var nums = [1, 2, 3, 4, 5, 6]
const isEven = n => n % 2 === 0
R.filter(isEven, nums);
R.reject(isEven, nums);

R.remove(1, 2, [1, 2, 3, 4, 5, 6]);

/*
 * 4. sort
 */
const ascend = R.subtract;
const decend = R.flip(R.subtract);
R.sort(ascend, [1, 4, 3, 2, 5]);
R.sort(decend, [1, 4, 3, 2, 5]);


const alice = {
  name: 'alice',
  age: 40
};
const bob = {
  name: 'bob',
  age: 30
};
const clara = {
  name: 'clara',
  age: 40
};
const bili = {
  name: 'bili',
  age: 30
}
const people = [clara, bob, alice, bili];

const ageNameSort = R.sortWith([
  R.descend(R.prop('age')), // 年龄降序, 优先
  R.ascend(R.prop('name'))  // name升序
]);
ageNameSort(people)