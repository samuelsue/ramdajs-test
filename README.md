# Ramdajs

学习下RamdaJS，代码建议使用VS Code配合Node.js REPL插件食用

打开文件，`ctrl+shift+p`(或者`ctrl+p`，每个人键位不一样)，选择`Node.js Interactive window (REPL) at selected file`

# Javascript 函数式编程

[课程地址 ](https://www.bilibili.com/video/BV1Nu411S7Jy) 作者: [ Kyle Simpson](https://book.douban.com/search/Kyle Simpson)

# 1. Introduction

## 为什么要函数式编程

**IMPERATIVE(命令式)VSDECLARATVE(声明式)**

假如给你一个list，里面都是数字，要你求和，大部分人都会想到同样的代码片段，先整个累加计数变量，整个for循环，循环这个list，取出数累加。你的同事你的上司或者你的后继者，看你这段屎一样的代码时，并不能一眼看出来这个循环是干啥的，那他就得把所有代码都看完，这就是IMPERATIVE，只关注代码执行。

什么最适合去执行代码，computer！什么不合适读代码，你的脑子！

声明式就是让你更多的去关注 这个代码是干啥的(为什么需要它)，结果是啥。(大佬插了一嘴，如果声明式代码还不满足直观的阅读，那就需要注释去补充这些东西，顺便举了个例子)

```javascript
var i = 0
// .... 你的一坨屎一样的逻辑
i++  // i + 1  这就是屎一样的注释

/*
上面i++的注释就是屎一样的注释，因为人一眼就能知道这代码是干啥的
而正确的注释，应该是说明为啥要i++, 为啥要i+1,跟+2有啥区别
要不然就别写注释，别人读起来更加费劲
*/
```

## Functional Programming Journey

大佬说，学习FP很困难，他学习&练习了3年，直到现在也不敢说自己是一个functional programmer，不过他鼓励你们好好努力，不要放弃，很多人在接触(Imperative)编程时候，觉得自己代码可读性还挺好，后面开始慢慢写FP，觉得代码怎么越写越屎，直到绝望放弃。大佬说这就是你的FP编程逐渐变好的开始。**Just Keep Pushing!!!**

## Code is Provable(可证明的)

大佬说，FP本质上是数学，但是，他不懂数学(😊，我信你个鬼，你个糟老头子坏得很)，估计听这个课的人也没几个人懂(🤣)

# 2. Function

## Function Naming Semantics(函数命名语义化)

Function应该是什么，描述输入与输出关系的语义化程序。

Function必须要有返回值(js函数默认的返回值为undefine)，大佬举了个例子，假如一个函数接收了一个obj，然后返回它的特定属性，如果这个obj不存在这个属性，那么调用函数得到的将是undefined

## Side Effects

到底什么是Side Effects，在FP里面，function要有着明确的输入和输出，所有间接的输入和输出(比如访问/修改了非自身作用域的变量)都是Side Effects，所以，应该**减少(由函数调用引起的)Side Effects**

因为不可能避免Side Effects

+ I/O, console
+ DOM
+ Database write
+ Timestamp
+ Random number
+ Networking

如果存在完全没有Side Effect的程序，那就没有办法证明这段程序存在，即你输入了一段数据到程序里，然后什么Side Effects都没有产生。

## Pure Functions & Constans

大佬给了一个例子来解释纯函数和常量之间的区别

```javascript
const z = 1;
// pure
function addTwo(x,y) {
    return x+y
}
function addAnother(x,y) {
    return addTwo(x,y) + z // inpure
}
addAnother(1,2); // 4
```

`addAnother`是个纯函数么？从严格的定义上来说，它不是，因为它访问了`z`，但是，从使用的角度来说，它和纯函数的作用一样，因为`z`是一个`const`，在代码执行的所有时间内，`z`都不会改变.

虽然`addAnother`是一个功能意义上的纯函数，但是这是因为我们阅读了它所有的相关代码，跟踪到`z`是一个永远不会改变的值，这样太费劲了，假如我们要证明一个复杂的函数是纯函数，那么我们要分析它所有的执行逻辑，判断它引用的变量的位置以及是否可能被修改，那这就太费劲了，所以这就是为什么纯函数需要严格的定义

## Reducecing Surface Area

还是上面那个例子，`z`可能被修改的地方有很多行，这就是**Surface Area**.

换一种写法

```javascript
function addAnother(z) {
    return function(x,y) {
        return x+y+z;
    }
}
addAnother(1)(1,2) // 4
```

这就减少了Surface Area， 因为`z`有可能被修改的地方只有2行，这就更加容易让人相信，`z`不会被修改，代码会更加可读

## Same Input, Same Output

```javascript
// 给你这么一个函数，你觉得它pure么
function getId(obj) {
    return obj.id
}
```

你觉得它输入多次同样的值，得到的结果都是一样的么😏？

如果你觉得是的话：

```javascript
getId({
    get id() {
        return Math.random() //
    }
})
```

慌了么？😏

其实，大佬想说的是，`getId`是纯的，但是函数调用应该是独立的，在拿`obj.id`的时候，调用了`id`的`getter`，从而引入了不纯的随机数。